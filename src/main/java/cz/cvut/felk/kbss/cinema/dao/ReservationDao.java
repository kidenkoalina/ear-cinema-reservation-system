package cz.cvut.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.model.Person;
import cz.cvut.felk.kbss.cinema.model.Projection;
import cz.cvut.felk.kbss.cinema.model.Reservation;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class ReservationDao extends BaseDao<Reservation> {
    public ReservationDao(){
        super(Reservation.class);
    }

    public List<Reservation> findAll(Integer id){
        Objects.requireNonNull(id);
        return em.createNamedQuery("Reservation.findAll", Reservation.class).getResultList();
    }
    public Reservation findByCustomer(Person customer){
        Objects.requireNonNull(customer);
        return em.createNamedQuery("Reservation.findByCustomer", Reservation.class).getSingleResult();
    }

    public List<Reservation> findByProjection(Projection projection) {
        Objects.requireNonNull(projection);
        return em.createNamedQuery("Reservation.findByProjection", Reservation.class).setParameter("projection", projection)
                .getResultList();
    }

    public List<Reservation> findByPerson(Person person) {
        Objects.requireNonNull(person);
        return em.createNamedQuery("Reservation.findByPerson", Reservation.class).setParameter("person", person)
                .getResultList();
    }
}
