package cz.cvut.felk.kbss.cinema.service;

import cz.cvut.felk.kbss.cinema.model.Employee;
import cz.cvut.felk.kbss.cinema.model.Person;
import cz.cvut.felk.kbss.cinema.model.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;

@Component
public class SystemInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInitializer.class);

    /**
     * Default admin username
     */
    private static final String ADMIN_USERNAME = "ear-admin@kbss.felk.cvut.cz";

    private final PersonService personService;

    private final PlatformTransactionManager txManager;

    @Autowired
    public SystemInitializer(PersonService personService,
                             PlatformTransactionManager txManager) {
        this.personService = personService;
        this.txManager = txManager;
    }

    @PostConstruct
    private void initSystem() {
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute((status) -> {
            generateAdmin();
            return null;
        });
    }

    /**
     * Generates an admin account if it does not already exist.
     */
    private void generateAdmin() {
        if (personService.exists(ADMIN_USERNAME)) {
            return;
        }
        final Employee admin = new Employee();
        admin.setEmail(ADMIN_USERNAME);
        admin.setName("System");
        admin.setSurname("Administrator");
        admin.setPassword("adm1n");
        admin.setRole(Role.ADMIN);
        admin.setEmployeeNumber(123);
        LOG.info("Generated admin user with credentials " + admin.getUsername() + "/" + admin.getPassword());
        personService.persist(admin);
    }
}
