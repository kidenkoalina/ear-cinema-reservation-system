package cz.cvut.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.model.Cinema;
import cz.cvut.felk.kbss.cinema.model.Cinemahall;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class CinemaHallDao extends BaseDao<Cinemahall>{
    public CinemaHallDao(){
        super(Cinemahall.class);
    }
}
