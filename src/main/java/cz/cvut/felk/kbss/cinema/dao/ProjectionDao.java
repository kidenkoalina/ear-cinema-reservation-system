package cz.cvut.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.model.Cinema;
import cz.cvut.felk.kbss.cinema.model.Cinemahall;
import cz.cvut.felk.kbss.cinema.model.Movie;
import cz.cvut.felk.kbss.cinema.model.Projection;
import org.postgresql.replication.fluent.physical.PhysicalReplicationOptions;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class ProjectionDao extends BaseDao<Projection> {

    public ProjectionDao() {
        super(Projection.class);
    }

    public List<Projection> findAll(Integer id){
        Objects.requireNonNull(id);
        return em.createNamedQuery("Projection.findAll", Projection.class).getResultList();
    }
    public List<Projection> findByMovie(Movie movie) {
        Objects.requireNonNull(movie);
        return em.createNamedQuery("Projection.findByMovie", Projection.class).setParameter("movie", movie)
                .getResultList();
    }
    public List<Projection> findByCinema(Cinema cinema){
        Objects.requireNonNull(cinema);
        return em.createNamedQuery("Projection.findByCinema", Projection.class).setParameter("cinema", cinema)
                .getResultList();
    }
    public List<Projection> getProjectionsByDate(LocalDate date){
        Objects.requireNonNull(date);
        return em.createNamedQuery("Projection.getProjectionsByDate", Projection.class).setParameter("date", date)
                .getResultList();
    }

    public List<Projection> getCurrentProjections(LocalDate date){
        Objects.requireNonNull(date);
        return em.createNamedQuery("Projection.getCurrentProjections", Projection.class).setParameter("date", date)
                .getResultList();
    }
}
