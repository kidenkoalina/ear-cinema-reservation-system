package cz.cvut.felk.kbss.cinema.dao;
import cz.cvut.felk.kbss.cinema.model.Cinema;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class CinemaDao extends BaseDao<Cinema>{
    public CinemaDao(){
        super(Cinema.class);
    }

    public Cinema findByName(String cinemaName) {
        try {
            return em.createNamedQuery("Cinema.findByName", Cinema.class).setParameter("name", cinemaName)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    public List<Cinema> findAll() {
        try {
            return em.createNamedQuery("Cinema.findAll", Cinema.class)
                    .getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }
}
