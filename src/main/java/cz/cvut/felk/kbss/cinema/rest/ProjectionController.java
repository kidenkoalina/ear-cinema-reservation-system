package cz.cvut.felk.kbss.cinema.rest;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.Cinema;
import cz.cvut.felk.kbss.cinema.model.Movie;
import cz.cvut.felk.kbss.cinema.model.Projection;
import cz.cvut.felk.kbss.cinema.model.Reservation;
import cz.cvut.felk.kbss.cinema.rest.util.RestUtils;
import cz.cvut.felk.kbss.cinema.service.CinemaService;
import cz.cvut.felk.kbss.cinema.service.MovieService;
import cz.cvut.felk.kbss.cinema.service.ProjectionService;
import cz.cvut.felk.kbss.cinema.service.ReservationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.PushBuilder;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("rest/projections")
public class ProjectionController {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectionController.class);

    private final ProjectionService projectionService;
    private final ReservationService reservationService;
    private final CinemaService cinemaService;
    private final MovieService movieService;

    @Autowired
    public ProjectionController(ProjectionService projectionService, ReservationService reservationService, CinemaService cinemaService, MovieService movieService){
        this.projectionService = projectionService;
        this.reservationService = reservationService;
        this.cinemaService = cinemaService;
        this.movieService = movieService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getProjections() { return projectionService.findAll(); }

    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getCurrentProjections(){
        LocalDate today = LocalDate.now();
        return projectionService.getCurrentProjections(today);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Projection getProjection(@PathVariable Integer id){
        Projection projection = projectionService.find(id);
        if(projection == null){
            throw NotFoundException.create("Projection", id);
        }
        return projection;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    @GetMapping(value = "/{id}/reservations", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getReservationsByProjection(@PathVariable Integer id){
        final Projection projection = projectionService.find(id);
        if(projection == null){
            throw NotFoundException.create("Projection", id);
        }
        return reservationService.findByProjection(projection);
    }

    @GetMapping(value = "/date/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getProjectionsByDate(@PathVariable String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(date, formatter);
        return projectionService.findByDate(localDate);
    }

    @GetMapping(value = "/cinema/{cinemaName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getProjectionsByCinema(@PathVariable String cinemaName){
        final Cinema cinema = cinemaService.findByName(cinemaName);
        return projectionService.findByCinema(cinema);
    }

    @GetMapping(value = "/movie/{movieName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getProjectionsByMovie(@PathVariable String movieName){
        final Movie movie = movieService.findByName(movieName);
        return projectionService.findByMovie(movie);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    @GetMapping(value = "/{id}/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getStatisticsByProjection(@PathVariable Integer id){
        final Projection projection = projectionService.find(id);
        final List<Reservation> reservations = reservationService.findByProjection(projection);
        Integer ticketsSum = 0;
        for (Reservation r: reservations) {
            if(r.isConfirmed()){ ticketsSum += r.getTickets(); }
        }
        return "Number of sold tickets is " + ticketsSum + " !";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addNewProjection(@RequestBody Projection p){
        projectionService.persist(p);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", p.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateProjection(@PathVariable Integer id, @RequestBody Projection projection) {
        final Projection original = getProjection(id);
        if (!original.getId().equals(projection.getId())) {
            throw new ValidationException("Projection identifier in the data does not match the one in the request URL.");
        }
        projectionService.update(projection);
        LOG.debug("Updated projection {}.", projection);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}/cancelUnconfirmedReservations")
    @ResponseStatus(HttpStatus.OK)
    public void cancelUnconfirmedReservationsByProjection(@PathVariable Integer id){
        Projection projection = projectionService.find(id);
        if(projection == null){
            throw NotFoundException.create("Projection", id);
        }
        reservationService.cancelUnconfirmedReservation(projection);
    }
}
