package cz.cvut.felk.kbss.cinema.rest;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.Cinema;
import cz.cvut.felk.kbss.cinema.model.Movie;
import cz.cvut.felk.kbss.cinema.model.Projection;
import cz.cvut.felk.kbss.cinema.rest.util.RestUtils;
import cz.cvut.felk.kbss.cinema.service.MovieService;
import cz.cvut.felk.kbss.cinema.service.ProjectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/movies")
public class MovieController {

    private static final Logger LOG = LoggerFactory.getLogger(MovieController.class);

    private final MovieService movieService;
    private final ProjectionService projectionService;

    @Autowired
    public MovieController(MovieService movieService, ProjectionService projectionService){
        this.movieService = movieService;
        this.projectionService = projectionService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Movie> getMovies(){
        return movieService.findAll();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addNewMovie(@RequestBody Movie m){
        movieService.persist(m);
        //TODO:pridat LOGGER
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", m.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Movie getMovie(@PathVariable Integer id) {
        final Movie movie = movieService.find(id);
        if (movie == null) {
            throw NotFoundException.create("Movie", id);
        }
        return movie;
    }

    @GetMapping(value = "/{id}/projections", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getProjectionsByMovie(@PathVariable Integer id){
        final Movie movie = movieService.find(id);
        if(movie == null){
            throw NotFoundException.create("Movie", id);
        }
        return projectionService.findByMovie(movie);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateMovie(@PathVariable Integer id, @RequestBody Movie movie) {
        final Movie original = getMovie(id);
        if (!original.getId().equals(movie.getId())) {
            throw new ValidationException("Movie identifier in the data does not match the one in the request URL.");
        }
        movieService.update(movie);
        LOG.debug("Updated movie {}.", movie);
    }
}
