package cz.cvut.felk.kbss.cinema.rest;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.Employee;
import cz.cvut.felk.kbss.cinema.model.Person;
import cz.cvut.felk.kbss.cinema.model.Reservation;
import cz.cvut.felk.kbss.cinema.rest.util.RestUtils;
import cz.cvut.felk.kbss.cinema.security.model.AuthenticationToken;
import cz.cvut.felk.kbss.cinema.service.PersonService;
import cz.cvut.felk.kbss.cinema.service.ReservationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/rest/persons")
public class PersonController {

    private static final Logger LOG = LoggerFactory.getLogger(PersonController.class);

    private final PersonService personService;
    private final ReservationService reservationService;


    @Autowired
    public PersonController(PersonService personService, ReservationService reservationService) {
        this.personService = personService;
        this.reservationService = reservationService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Person> getPersons(){
        return personService.findAll();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> registerPerson(@RequestBody Person p){
        personService.persist(p);
        LOG.debug("Person {} successfully registered.", p);
//        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", p.getId());
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PostMapping(value = "/emp",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> registerEmployee(@RequestBody Employee p){
        personService.persist(p);
        LOG.debug("Employee {} successfully registered.", p);
//        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", p.getId());
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Person getPerson(@PathVariable Integer id) {
        final Person person = personService.find(id);
        if (person == null) {
            throw NotFoundException.create("Person", id);
        }
        return person;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_PERSON', 'ROLE_EMPLOYEE')")
    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public Person getCurrent(Principal principal) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        return auth.getPrincipal().getUser();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePerson(@PathVariable Integer id, @RequestBody Person person) {
        final Person original = getPerson(id);
        if (!original.getId().equals(person.getId())) {
            throw new ValidationException("Person identifier in the data does not match the one in the request URL.");
        }
        personService.update(person);
        LOG.debug("Updated person {}.", person);
    }

    @GetMapping(value = "/{id}/reservations", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getReservationsByPerson(@PathVariable Integer id) {
        final Person person = getPerson(id);
        return reservationService.findByPerson(person);
    }




}
