package cz.cvut.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class EmployeeDao extends BaseDao<Employee>{
    public EmployeeDao(){
        super(Employee.class);
    }
}
