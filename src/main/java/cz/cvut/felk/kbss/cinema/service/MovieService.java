package cz.cvut.felk.kbss.cinema.service;

import cz.cvut.felk.kbss.cinema.dao.MovieDao;
import cz.cvut.felk.kbss.cinema.model.Cinema;
import cz.cvut.felk.kbss.cinema.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class MovieService {

    private final MovieDao movieDao;

    @Autowired
    public MovieService(MovieDao movieDao){
        this.movieDao = movieDao;
    }

    @Transactional(readOnly = true)
    public List<Movie> findAll(){
        return movieDao.findAll();
    }

    @Transactional
    public void persist(Movie cinema) {
        Objects.requireNonNull(cinema);
        movieDao.persist(cinema);
    }

    @Transactional
    public void update(Movie movie) {
        movieDao.update(movie);
    }

    @Transactional(readOnly = true)
    public Movie find(Integer id) {
        return movieDao.find(id);
    }

    @Transactional
    public Movie findByName(String name) {
        return movieDao.findByName(name);
    }

    @Transactional(readOnly = true)
    public boolean exists(String username) {
        return movieDao.findByName(username) != null;
    }
}
