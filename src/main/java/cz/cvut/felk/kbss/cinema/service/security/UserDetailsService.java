package cz.cvut.felk.kbss.cinema.service.security;

import cz.cvut.felk.kbss.cinema.dao.PersonDao;
import cz.cvut.felk.kbss.cinema.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 10.cviko
 * @author kidenkoalina on 11/12/2019
 */
@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final PersonDao personDao;

    @Autowired
    public UserDetailsService(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        final Person person = personDao.findByEmail(email);
        if (person == null) {
            throw new UsernameNotFoundException("User with username " + email + " not found.");
        }
        return new cz.cvut.felk.kbss.cinema.security.model.UserDetails(person);
    }
}
