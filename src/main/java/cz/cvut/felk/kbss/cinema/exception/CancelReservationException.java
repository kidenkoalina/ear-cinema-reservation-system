package cz.cvut.felk.kbss.cinema.exception;

public class CancelReservationException extends EarException {
        public CancelReservationException(String message) { super(message);}
}
