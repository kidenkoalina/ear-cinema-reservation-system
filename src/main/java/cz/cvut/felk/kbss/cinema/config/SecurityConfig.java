package cz.cvut.felk.kbss.cinema.config;

import cz.cvut.felk.kbss.cinema.rest.component.RestAuthenticationEntryPoint;
import cz.cvut.felk.kbss.cinema.rest.handler.RequestAuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)// Allow methods to be secured using annotation
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthenticationFailureHandler authenticationFailureHandler;

    private final RequestAuthenticationSuccessHandler requestAuthenticationSuccessHandler;

    private final AuthenticationProvider authenticationProvider;

    private final RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    public SecurityConfig(AuthenticationFailureHandler authenticationFailureHandler,
                          RequestAuthenticationSuccessHandler requestAuthenticationSuccessHandler,
                          AuthenticationProvider authenticationProvider,
                          RestAuthenticationEntryPoint restAuthenticationEntryPoint) {
        this.authenticationFailureHandler = authenticationFailureHandler;
        this.requestAuthenticationSuccessHandler = requestAuthenticationSuccessHandler;
        this.authenticationProvider = authenticationProvider;
        this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint)
                .and().authorizeRequests().anyRequest().permitAll()
                .and().formLogin().successHandler(requestAuthenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .and().httpBasic()
                .and().logout();
    }
}
