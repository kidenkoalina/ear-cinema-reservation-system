package cz.cvut.felk.kbss.cinema.exception;

public class ConfirmReservationException extends EarException {
    public ConfirmReservationException(String message) { super(message);}
}
