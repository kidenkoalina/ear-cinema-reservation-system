package cz.cvut.felk.kbss.cinema.rest;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.Movie;
import cz.cvut.felk.kbss.cinema.model.Reservation;
import cz.cvut.felk.kbss.cinema.rest.util.RestUtils;
import cz.cvut.felk.kbss.cinema.service.ReservationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/reservations")
public class ReservationController {

    private static final Logger LOG = LoggerFactory.getLogger(ReservationController.class);

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    public List<Reservation> getReservations() {
        return reservationService.findAll();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_PERSON', 'ROLE_EMPLOYEE')")
    public ResponseEntity<String> addNewReservation(@RequestBody Reservation r){
        reservationService.addNewReservation(r);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", r.getId());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    public Reservation getReservation(@PathVariable Integer id) {
        final Reservation reservation = reservationService.find(id);
        if (reservation == null) {
            throw NotFoundException.create("Reservation", id);
        }
        return reservation;
    }

    @PutMapping(value = "/{id}/confirm")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    public void confirmReservation(@PathVariable Integer id){
        final Reservation reservation = reservationService.find(id);
        reservationService.confirmReservation(reservation);
    }

    @DeleteMapping(value = "/{id}/cancel")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_PERSON', 'ROLE_EMPLOYEE')")
    public void cancelReservation(@PathVariable Integer id){
        final Reservation reservation = reservationService.find(id);
        reservationService.cancelReservation(reservation);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateReservation(@PathVariable Integer id, @RequestBody Reservation reservation) {
        final Reservation original = getReservation(id);
        if (!original.getId().equals(reservation.getId())) {
            throw new ValidationException("Reservation identifier in the data does not match the one in the request URL.");
        }
        reservationService.update(reservation);
        LOG.debug("Updated reservation {}.", reservation);
    }
}
