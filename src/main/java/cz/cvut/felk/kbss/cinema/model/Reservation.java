/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.felk.kbss.cinema.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "reservation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reservation.findAll", query = "SELECT r FROM Reservation r")
    , @NamedQuery(name = "Reservation.findByProjection", query = "SELECT r from Reservation r WHERE r.projection = :projection")
    , @NamedQuery(name = "Reservation.findByPerson", query = "SELECT r FROM Reservation r WHERE r.person = :person")})
public class Reservation implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "person", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Person person;

    @JoinColumn(name = "projection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Projection projection;

    @NotNull
    @Column(name = "tickets")
    private short tickets;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private StateOfReservation stateOfReservation;

    public Reservation() { }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Projection getProjection() {
        return projection;
    }

    public void setProjection(Projection projection) {
        this.projection = projection;
    }

    public short getTickets() {
        return tickets;
    }

    public void setCancelled() {
        this.stateOfReservation = StateOfReservation.CANCELLED;
    }

    public StateOfReservation getStateOfReservation() {
        return stateOfReservation;
    }

    public void setStateOfReservation(StateOfReservation stateOfReservation) {
        this.stateOfReservation = stateOfReservation;
    }

    public void setConfirmed() {
        this.stateOfReservation = StateOfReservation.CONFIRMED;
    }

    public boolean isCancelled() {
        return this.stateOfReservation == StateOfReservation.CANCELLED;
    }

    public boolean isConfirmed() {
        return this.stateOfReservation == StateOfReservation.CONFIRMED;
    }

    public void setTickets(short count){
        this.tickets = count;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "projection = '" + getProjection() + "', person = " + getPerson() +
                "}";
    }
}
