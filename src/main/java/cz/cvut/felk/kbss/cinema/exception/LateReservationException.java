package cz.cvut.felk.kbss.cinema.exception;

public class LateReservationException extends EarException {
    public LateReservationException(String message) { super(message);}
}
