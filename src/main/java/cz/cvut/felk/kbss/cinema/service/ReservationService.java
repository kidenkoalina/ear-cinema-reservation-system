package cz.cvut.felk.kbss.cinema.service;

import cz.cvut.felk.kbss.cinema.dao.CinemaHallDao;
import cz.cvut.felk.kbss.cinema.dao.PersonDao;
import cz.cvut.felk.kbss.cinema.dao.ProjectionDao;
import cz.cvut.felk.kbss.cinema.dao.ReservationDao;
import cz.cvut.felk.kbss.cinema.exception.CancelReservationException;
import cz.cvut.felk.kbss.cinema.exception.ConfirmReservationException;
import cz.cvut.felk.kbss.cinema.exception.InsufficientAmountException;
import cz.cvut.felk.kbss.cinema.exception.LateReservationException;
import cz.cvut.felk.kbss.cinema.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

@Service
public class  ReservationService {

    private final ReservationDao reservationDao;

    private final PersonDao personDao;

    private final ProjectionDao projectionDao;

    private final CinemaHallDao cinemaHallDao;

    @Autowired
    public ReservationService(ReservationDao reservationDao, PersonDao personDao, ProjectionDao projectionDao, CinemaHallDao cinemaHallDao) {
        this.reservationDao = reservationDao;
        this.personDao = personDao;
        this.projectionDao = projectionDao;
        this.cinemaHallDao = cinemaHallDao;
    }

    @Transactional(readOnly = true)
    public List<Reservation> findAll(){
        return reservationDao.findAll();
    }

    @Transactional
    public void persist(Reservation reservation) {
        Objects.requireNonNull(reservation);
        reservationDao.persist(reservation);
    }

    @Transactional
    public void update(Reservation reservation) {
        reservationDao.update(reservation);
    }

    @Transactional(readOnly = true)
    public Reservation find(Integer id) {
        return reservationDao.find(id);
    }

    @Transactional(readOnly = true)
    public List<Reservation> findByProjection(Projection projection){
        return reservationDao.findByProjection(projection);
    }

    @Transactional(readOnly = true)
    public List<Reservation> findByPerson(Person person){
        return reservationDao.findByPerson(person);
    }

    @Transactional
    public void addNewReservation(Reservation reservation) {
        reservation.setStateOfReservation(StateOfReservation.CREATED);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final String username = auth.getName();
        Person loggedPerson = personDao.findByEmail(username);

        int personId = reservation.getPerson().getId();
        if(loggedPerson.isPerson() && loggedPerson.getId() != personId){
            throw new InsufficientAmountException("You can't create reservation for other people");
        }
//        Person person = personDao.find(personId);

        int projectionId = reservation.getProjection().getId();
        Projection projection = projectionDao.find(projectionId);

        //Can we do the reservation for given numver of tickets?
        int cinemahallId = projection.getCinemahall().getId();
        Cinemahall cinemahall = cinemaHallDao.find(cinemahallId);

        List<Reservation> reservations = findByProjection(projection);
        int alreadyReservedTickets = 0;
        for (Reservation r : reservations) {
            alreadyReservedTickets += r.getTickets();
        }

        if (!((cinemahall.getCapacity() - alreadyReservedTickets) >= reservation.getTickets())) {
            throw new InsufficientAmountException("You can't have so many tickets");
        }

        //Admin and Employee can do reservaion for any projection's date and time
        if(loggedPerson.isAdmin() || loggedPerson.isEmployee()){
            reservationDao.persist(reservation);
        }

        //Person must do reservation before 30 minutes before projection starts
        if(loggedPerson.isPerson()) {
            LocalDate nowDate = LocalDate.now();
            LocalDate projectionDate = projection.getDate();
            if(nowDate.isAfter(projectionDate)){
                throw new LateReservationException("You can't create reservation for passed projection");
            }
            if(nowDate.equals(projectionDate)){
                LocalTime nowTime = LocalTime.now();
                LocalTime projectonTime = projection.getTime();
                if(!nowTime.plusMinutes(30).isBefore(projectonTime)){
                    throw new LateReservationException("You can't create reservation if there is less than 30 minutes before projection");
                }
            }
            reservationDao.persist(reservation);
        }
    }

    @Transactional
    public void confirmReservation(Reservation reservation){
        if(reservation.isConfirmed()){
            throw new ConfirmReservationException("You can't confirm already confirmed reservation");
        }
        if(reservation.isCancelled()){
            throw new ConfirmReservationException("You can't confirm cancelled reservation");
        }
        reservation.setConfirmed();
        reservationDao.update(reservation);
    }

    @Transactional
    public void cancelReservation(Reservation reservation){
        if(reservation.isConfirmed()){
            throw new CancelReservationException("You can't cancel already confirmed reservation");
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final String username = auth.getName();
        Person loggedPerson = personDao.findByEmail(username);

        if(loggedPerson.isAdmin() || loggedPerson.isEmployee()){
            reservationDao.persist(reservation);
        }

        if(loggedPerson.getId() != reservation.getPerson().getId()){
            throw new CancelReservationException("You can't cancel reservation which doesn't belong to you");
        }
        reservation.setCancelled();
        reservationDao.update(reservation);
    }

    @Transactional
    public void cancelUnconfirmedReservation(Projection projection) {
        List<Reservation> reservations = reservationDao.findByProjection(projection);
        for (Reservation r:reservations) {
            if(!r.isConfirmed()){
//                cancelReservation(r);
            }
        }
    }
}
