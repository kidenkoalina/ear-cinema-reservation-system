package cz.cvut.felk.kbss.cinema.felk.kbss.cinema.service;

import cz.cvut.felk.kbss.cinema.exception.CancelReservationException;
import cz.cvut.felk.kbss.cinema.exception.InsufficientAmountException;
import cz.cvut.felk.kbss.cinema.exception.LateReservationException;
import cz.cvut.felk.kbss.cinema.felk.kbss.cinema.environment.Generator;
import cz.cvut.felk.kbss.cinema.model.*;
import cz.cvut.felk.kbss.cinema.security.DefaultAuthenticationProvider;
import cz.cvut.felk.kbss.cinema.security.model.UserDetails;
import cz.cvut.felk.kbss.cinema.service.PersonService;
import cz.cvut.felk.kbss.cinema.service.ReservationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ReservationServiceTest{

    @PersistenceContext
    private EntityManager em;

    @Autowired
    ReservationService reservationService;

    @Autowired
    private PersonService personService;

    private Person person;
    private String rawPassword;


    @Autowired
    private DefaultAuthenticationProvider provider;

    @Before
    public void setUp() {
        person = Generator.generatePerson();
        rawPassword = person.getPassword();
        personService.persist(person);
        em.persist(person);
//        em.persist(person);
        SecurityContextHolder.setContext(new SecurityContextImpl());
    }


    private void auth(){
        final Authentication auth = new UsernamePasswordAuthenticationToken(person.getUsername(), rawPassword);
        final SecurityContext context = SecurityContextHolder.getContext();
        assertNull(context.getAuthentication());
        final Authentication result1 = provider.authenticate(auth);
        assertNotNull(SecurityContextHolder.getContext());
        final UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        person.setPassword("123");
    }

    @Test
    public void addNewReservationAddsReservation(){

        auth();

        final Reservation reservation = new Reservation();
        reservation.setTickets((short) 1);
        reservation.setPerson(person);

        final Projection projection = generateProjection();
        reservation.setProjection(projection);
        reservation.setStateOfReservation(StateOfReservation.CREATED);
        reservationService.addNewReservation(reservation);

        final Reservation result = em.find(Reservation.class, reservation.getId());

        assertEquals(reservation.getId(), result.getId());
        assertEquals(reservation.getProjection().getId(), result.getProjection().getId());

    }

    @Test(expected = InsufficientAmountException.class)
    public void addNewReservationThrowsInsufficientAmountExceptionWhenNotEnoughTickets(){

        auth();

        final Projection projection = generateProjection();

        Reservation reservation = new Reservation();
        reservation.setPerson(person);
        reservation.setProjection(projection);
        reservation.setTickets((short) 2);
        reservationService.addNewReservation(reservation);
    }

    @Test(expected = LateReservationException.class)
    public void addNewReservationThrowsLateReservationExceptionWhen30MinutesOrLessBeforeProjection(){

        auth();

        final Projection projection = generateProjection();
        LocalTime time = LocalTime.now().plusMinutes(20);
        projection.setTime(time);
        em.persist(projection);

        Reservation reservation = new Reservation();
        reservation.setPerson(person);
        reservation.setProjection(projection);
        reservationService.addNewReservation(reservation);
    }

    @Test(expected = CancelReservationException.class)
    public void cancelReservationThrowsCancelReservationExceptionForConfirmedReservation(){
        Reservation reservation = new Reservation();
        reservation.setPerson(person);
        reservation.setConfirmed();
        reservationService.cancelReservation(reservation);
    }

    private Projection generateProjection(){
        final Projection projection = new Projection();

        final Cinema cinema = new Cinema();
        cinema.setName("Cinema");
        cinema.setTown("Town");
        cinema.setId(10);
        em.persist(cinema);

        final Cinemahall cinemahall = new Cinemahall();
        cinemahall.setCapacity((short) 1);
        cinemahall.setNumber((short) 4);
        cinemahall.setCinema(cinema);
        em.persist(cinemahall);

        projection.setCinemahall(cinemahall);

        LocalDate date = LocalDate.now();
        projection.setDate(date);

        LocalTime time = LocalTime.now().plusMinutes(60);

        projection.setTime(time);
        em.persist(projection);

        return projection;
    }
}