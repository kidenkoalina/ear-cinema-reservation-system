package cz.cvut.felk.kbss.cinema.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.SpringbootDemoApplication;
import cz.cvut.felk.kbss.cinema.dao.ProjectionDao;
import cz.cvut.felk.kbss.cinema.felk.kbss.cinema.environment.Generator;
import cz.cvut.felk.kbss.cinema.model.Movie;
import cz.cvut.felk.kbss.cinema.model.Projection;
//import jdk.internal.agent.AgentConfigurationError;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan(basePackageClasses = SpringbootDemoApplication.class)
public class ProjectionDaoTest {

    @Autowired
    private EntityManager em;

    @Autowired
    ProjectionDao projectionDao;

    @Test
    public void findByMovieReturnsProjectionsWithMatchingMovie() {
        final Movie movie = generateMovie();

        //vygeneruje list projekci pro dane Movie
        final List<Projection> projections = generateProjectionsWithMovie(movie);

        final List<Projection> result = projectionDao.findByMovie(movie);
        assertEquals(projections.size(), result.size());
        projections.sort(Comparator.comparing(Projection::getId));
        result.sort(Comparator.comparing(Projection::getId));
        for(int i = 0; i < projections.size(); i++){
            assertEquals(projections.get(i).getId(), result.get(i).getId());
        }
    }

    @Test
    public void findByDateReturnsProjectionsWithMatchingDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        final LocalDate date = LocalDate.parse("2020-01-20", formatter);

        //vygeneruje list projekci pro dany Date
        final List<Projection> projections = generateProjectionsWithDate(date);

        final List<Projection> result = projectionDao.getProjectionsByDate(date);
        assertEquals(projections.size(), result.size());
        projections.sort(Comparator.comparing(Projection::getId));
        result.sort(Comparator.comparing(Projection::getId));
        for(int i = 0; i < projections.size(); i++){
            assertEquals(projections.get(i).getId(), result.get(i).getId());
        }
    }


    private List<Projection> generateProjectionsWithMovie(Movie movie){
        List<Projection> projections = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Projection p = generateProjectionWithMovie(movie);
            projections.add(p);
        }
        return  projections;
    }

    private List<Projection> generateProjectionsWithDate(LocalDate date){
        List<Projection> projections = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Projection p = generateProjectionWithDate(date);
            projections.add(p);
        }
        return  projections;
    }

    private Projection generateProjectionWithMovie(Movie movie){
        final Projection projection = new Projection();
        projection.setMovie(movie);
        projection.setDabing("CZ");
        projection.setDate(LocalDate.now().plusDays(15));
        projection.setTime(LocalTime.now());
        projection.setPrice(150);
        em.persist(projection);
        return projection;
    }

    private Projection generateProjectionWithDate(LocalDate date){
        final Projection projection = new Projection();
        projection.setMovie(generateMovie());
        projection.setDabing("CZ");
        projection.setDate(date);
        projection.setTime(LocalTime.now());
        projection.setPrice(150);
        em.persist(projection);
        return projection;
    }

    public Movie generateMovie(){
        final Movie movie = new Movie();
        movie.setName("TestMovie");
        movie.setDescription("TestDescription");
        movie.setAgeRate((short) 6);
        movie.setLength((short) 120);
        em.persist(movie);
        return movie;
    }
}
